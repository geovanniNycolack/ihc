<section>
	<h1>Informe o DOI</h1>
	<p>Informe o DOI do seu artigo</p>
	<div class="form-group">
		<input type="email" class="form-control" id="DOI" aria-describedby="DoiSmall" placeholder="DOI">
		<small id="DoiSmall" class="form-text text-muted"></small>
	</div>
	<button type="submit" id='next'class="btn btn-primary">Proxímo</button>
</section>

<script type="text/javascript">
	$("#next").click(function (e) {
		$.get( "parts/pages/art/2.php", function( data ) {
		  $( "#content" ).html( data );
		});
	});
</script>
