<section>
	<h1>Outros idiomas</h1>
	<p>Informe o titulo e o resumo em outro idioma</p>
	<div class="form-group">
		<input type="text" class="form-control" id="Nome" placeholder="Titulo">
	</div>
	<div class="form-group">
	    <label for="exampleTextarea">Informações Adicionais</label>
	    <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
	  </div>

	<button type="submit" id='next'class="btn btn-primary">Concluir</button>
</section>

<script type="text/javascript">
	$("#next").click(function (e) {
		$.get( "parts/pages/art/4.php", function( data ) {
		  $( "#content" ).html( data );
		});
	});
</script>
