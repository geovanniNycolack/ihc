<section>
	<h1>Dados Gerais</h1>
	<div class="form-group">
		<label for="Nome">Nome</label>
		<input type="text" class="form-control" id="Nome" placeholder="Nome">
	</div>
	<div class="form-group">
		<label for="Nome">Ano previsto para sua publicação</label>
		<input type="text" class="form-control" id="Nome" placeholder="Ano previsto">
	</div>
	<div class="form-group">
		<label class="mr-md-2" for="Idioma">Idioma</label>
		<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="Idioma">
	      <option selected>Escolha:</option>
	      <option value="1">Português</option>
	      <option value="2">Ingles</option>
	      <option value="3">Espanhol</option>
		  <option value="4">Russo</option>
	    </select>
	</div>

		<fieldset class="form-group row">
         <legend class="col-form-legend col-sm-12">
			 É um dos 5 trabalhos mais relevantes de sua produção?
		 </legend>
         <div class="col-sm-10">
           <div class="form-check form-check-inline">
             <label class="form-check-label">
               <input class="form-check-input" type="radio" name="gridRadios" id="TrabalhoPrincipal1" value="Sim" checked>
               Sim
             </label>
           </div>
           <div class="form-check form-check-inline">
             <label class="form-check-label">
               <input class="form-check-input" type="radio" name="gridRadios" id="TrabalhoPrincipal2" value="Nao">
               Não
             </label>
           </div>
           </div>
       </fieldset>


	   <fieldset class="form-group row">
   	 <legend class="col-form-legend col-sm-12">
		É uma produção para educação e popularização de CeT?
	 </legend>
   	 <div class="col-sm-10">
   	   <div class="form-check form-check-inline">
   		 <label class="form-check-label">
   		   <input class="form-check-input" type="radio" name="gridRadios" id="TrabalhoPrincipal1" value="Sim" checked>
   		   Sim
   		 </label>
   	   </div>
   	   <div class="form-check form-check-inline">
   		 <label class="form-check-label">
   		   <input class="form-check-input" type="radio" name="gridRadios" id="TrabalhoPrincipal2" value="Nao">
   		   Não
   		 </label>
   	   </div>
   	   </div>
      </fieldset>


	<button type="submit" id='next'class="btn btn-primary">Proxímo</button>
</section>

<script type="text/javascript">
	$("#next").click(function(e) {
		$.get("parts/pages/art/3.php", function(data) {
			$("#content").html(data);
		});
	});
</script>
