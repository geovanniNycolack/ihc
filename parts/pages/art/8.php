<section>
	<h1>Outras informacoes</h1>

	<div class="form-group">
	    <label for="exampleTextarea">Informações Adicionais</label>
	    <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
	  </div>

	<button type="submit" id='next'class="btn btn-primary">Proxímo</button>
</section>

<script type="text/javascript">
	$("#next").click(function (e) {
		$.get( "parts/pages/art/9.php", function( data ) {
		  $( "#content" ).html( data );
		});
	});
</script>
