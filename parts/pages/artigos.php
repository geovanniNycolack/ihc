<section>
	<h1>Artigos publicados</h1>
	<p>Todos os artigos vinculados ao seu currículo</p>
	<table class="table">
		<thead>
			<tr>
				<th>Titulo</th>
				<th>Ano</th>
				<th>Idioma</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody id='values'>
			<tr>
				<th>Lorem Ipsum</th>
				<td>2001</td>
				<td>Portugues</td>
				<td><a href="#">Editar</a><br><a href="#">Remover</a></td>
			</tr>
			<tr>
				<th>Lorem Ipsum</th>
				<td>2007</td>
				<td>Portugues</td>
				<td><a href="#">Editar</a><br><a href="#">Remover</a></td>
			</tr>
			<tr>
				<th>Lorem Ipsum</th>
				<td>1998</td>
				<td>Portugues <br>Russo</td>
				<td><a href="#">Editar</a><br><a href="#">Remover</a></td>
			</tr>

		</tbody>
	</table>

	<button type="submit" id='next'class="btn btn-primary">Adicionar Novo</button>
</section>

<script type="text/javascript">
$("#next").click(function (e) {
	$.get( "parts/pages/art/1.php", function( data ) {
	  $( "#content" ).html( data );
	});
});
</script>
