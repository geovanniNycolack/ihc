<nav>
	<div class="container ">
		<div class="row">
			<div class="hidden-sm-down col-md-12 items">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dados Gerais<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#">ITEM</a></li>
						<li><a href="#">ITEM</a></li>
						<li><a href="#">ITEM</a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Formação<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Atuação<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Projetos<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Produções<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Patentes e registros<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Inovação<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Educação<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Eventos<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Orientações<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Bancas<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Citações<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li class="divider"></li>
					</ul>
				</li>

			</div>
		</div>
	</div>
</nav>
