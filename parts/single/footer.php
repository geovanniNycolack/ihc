<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul>
					<li><a href="#">Alto Contraste</a></li>
					<li><a href="" id="fontPlus">Fonte +</a></li>
					<li><a href="" id='fontMinus'>Fonte -</a></li>
					<li><a href="#">Ajuda</a></li>
				</ul>

			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul>
					<li>CNPq - Conselho Nacional de Desenvolvimento Científico e Tecnológico</li>
				</ul>
			</div>
		</div>
	</div>

</footer>


<script src="assets/js/popper.js" charset="utf-8"></script>
<script src="assets/js/bootstrap.min.js" charset="utf-8"></script>

<script src="assets/js/codes.min.js" charset="utf-8"></script>


</body>

</html>
