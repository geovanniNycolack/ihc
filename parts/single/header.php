<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9"><![endif]-->
<!--[if IE 8]><html class="preIE9"><![endif]-->
<!--[if gte IE 9]><!-->
<html id='html'>
<!--<![endif]-->

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>IHC</title>


	<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="assets/css/style.css" type="text/css">
	<link rel="stylesheet" href="assets/css/jquery-ui.min.css" type="text/css">
	<script src="assets/js/jquery-3.2.1.js" type="text/javascript"></script>
	<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>

</head>

<body>

	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<a href="index.php"><img src="assets/img/logo.png" alt="Logo"></a>
				</div>
				<div class="col-md-4">
					<div class="items" style="text-align: center;">
						<h1><a href="curriculo.php">Visualizar Currículo</a></h1>
						<h2>Atualizar Currículo</h2>
						<h3>Você tem 9 avisos</h3>
					</div>
				</div>
				<div class="col-md-4" style="text-align: Right;">
					<div class="">
						<a class='exit' href="#">Sair</a>
					</div>
				</div>
			</div>
		</div>
	</header>
