var fontSize = 16;
$("#fontPlus").click(function(evt) {
	evt.preventDefault();
	fontSize += 1;
	$("html").css("font-size", fontSize + 'px');
});


$("#fontMinus").click(function(evt) {
	evt.preventDefault();
	fontSize -= 1;
	$("html").css("font-size", fontSize + 'px');
});
