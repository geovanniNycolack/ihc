<?php
require 'parts/single/header.php';
require 'parts/single/nav.php';
?>

<article>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php require 'parts/single/sideNav.php' ?>
			</div>

			<div class="col-md-9" id="content">
				<?php require 'parts/pages/curriculo.php' ?>
			</div>
		</div>
	</div>
</article>

<?php require 'parts/single/footer.php' ?>
